-module(battleships).

-export([init_server/0, init_client/1, server/0, client/1]).

-define(MAP_ROWS, 10).
-define(MAP_COLS, 10).
-define(NUM_SHIPS, 3).

%
% Map:
% map{<cell> => <status>}
% <cell> : {row, col} : row and col as numbers 1 - N
% <status> : list of status values
%           ship        : own ship
%           ship_shot   : own ship shot by the enemy
%           shot_water  : own shot to the enemy, hitting just water
%           shot_hit    : own shot to the enemy, hitting a ship 
%
% Messages:
% (shot, cell)      : shot at the enemy  
% (hit, cell)       : a ship was hit  
% (water, cell)     : missed shot
% (victory, cell)   : all ships were hit
%


% Init an empy map with defined size and shape
% returns generated map
init_map() ->
    init_map(?MAP_ROWS, ?MAP_COLS, #{}).

init_map(N, M, Map) ->
    if 
        N == 0 -> 
            Map;
        M > 0 -> 
            init_map(N, M-1, maps:put({N,M}, [], Map));
        M == 0 ->
            init_map(N-1, ?MAP_ROWS, Map)
    end.


% Place a ship on the Map
% X     : number of ships to place, decremented every recursice call 
% Map   : map to place the ship to
% returns map with placed ships
place_ship(X, Map) ->
    if 
        X == 0 ->
            Map;
        X > 0 ->
            io:format("Still to place: ~2w~n", [X]),
            case io:fread("[p]lace <row> <col>: ", " ~a ~d ~d") of
                {ok, [p|T]} ->
                    Snew = lists:append(maps:get({lists:nth(1, T),lists:nth(2, T)}, Map), [ship]),
                    Mapnew = maps:update({lists:nth(1, T),lists:nth(2, T)}, Snew, Map),
                    print_map(Mapnew),
                    place_ship(X-1, Mapnew);
                true ->
                    io:format("Unknown command!~n"),
                    place_ship(X, Map)
            end
    end.


% Shoot at the enemy at PID
% Read target from standard_io and send per ProcessMessage to EnemyPID and
% waits for an answer to be received.
% Recursice call to itself if an enemy ship was hit, calls receive_shot if
% only water was hit and terminates if the last remaining enemy ship was hit.
% Map   : Map of this process to mark hit targets
% PID   : PID of the EnemyProcess
% returns none
shoot(Map, PID) ->
    % shoot at your enemy
    io:format("~n~nIt is your turn, shoot at your enemy!~n"),
    % read position for shot
    case io:fread("[s]hot <row> <col>: ", " ~a ~d ~d") of
        {ok, [s|T]} ->
            Target = {lists:nth(1, T),lists:nth(2, T)}
    end,
    S = maps:get(Target, Map),
    % send shotMessage to the other process 
    PID ! {shot, Target},
    % receive resultMessage
    receive
        {victory, {N,M}} ->
            Snew = lists:append(S, [shot_hit]),
            Map_new = maps:update({N,M}, Snew, Map),
            %output
            print_map(Map_new),
            io:format("You hit your enemy at ~p!~n", [{N,M}]),
            io:format("Your enemy is defeated, YOU WIN!~nGAME OVER~n"),
            % game over
            ok;

        {hit, {N,M}} ->
            Snew = lists:append(S, [shot_hit]),
            Map_new = maps:update({N,M}, Snew, Map),
            % output
            print_map(Map_new),
            io:format("You hit your enemy at ~p!~n", [{N,M}]),
            % hit, so shoot again
            shoot(Map_new, PID);

        {water, {N,M}} ->
            Snew = lists:append(S, [shot_water]),
            Map_new = maps:update({N,M}, Snew, Map),
            % output
            print_map(Map_new),
            io:format("You missed your enemy at ~p!~n", [{N,M}]),
            % missed, enemy shoots next
            receive_shot(Map_new, PID)
    end.


% Receive a shot from the EnemeyProcess with PID
% Evaluates the shot against the own map(Map), calls shoot if nothing was hit,
% else evaluate Map for remaining ships and calls receive shoot if there are any,
% terminates otherwise
% Map   : own Map
% PID   : PID of EnemyProcess
% returns none
receive_shot(Map, PID) ->
    % receive a shot from your enemy
    io:format("~n~nYour enemy shoots at you!~n"),
    receive
        {shot, {N,M}} ->
            {Map_new, Hit} = evaluate_shot({N,M}, Map),
            case Hit of
                true ->
                    case evaluate_game(Map_new) of
                        true -> 
                            PID ! {hit, {N,M}},
                            print_map(Map_new),
                            io:format("You were hit at ~p!~n", [{N,M}]),
                            receive_shot(Map_new, PID);
                        false ->
                            PID ! {victory, {N,M}},
                            print_map(Map_new),
                            io:format("You were hit at ~p!~n", [{N,M}]),
                            io:format("~nAll your ships were shot, YOU LOOSE!~nGAME OVER~n", [])
                    end;

                false ->
                    PID ! {water, {N,M}},
                    print_map(Map_new),
                    io:format("The enemy missed!~n", []),
                    shoot(Map_new, PID)
            end
    end.


% Evaluates a shot against the own map
% Returns tuple with updated map and boolean indicating if a ship was hit.
% {N,M} : Position to evaluate as {row,col}
% Map   : own Map
% returns {newMap, boolean}
evaluate_shot({N,M}, Map) ->
    S = maps:get({N,M}, Map),
    case {lists:member(ship, S),lists:member(ship_shot, S)} of
        {true,_} ->
            Hit = true,
            Snew = lists:append([ship_shot], lists:delete(ship, S));
        {_,true} ->
            Hit = true,
            Snew = lists:append([ship_shot], lists:delete(ship, S));
        {false,false} ->
            Hit = false,
            Snew = S
    end,
    {maps:update({N,M}, Snew, Map), Hit}.

% Evaluates game state by filtering the map for "ship",
% returns true if remaining ships found, false otherwise
% Map   : own Map
% returns boolean
evaluate_game(Map) ->
    maps:size(maps:filter(
    fun(_,V) -> 
        lists:member(ship, V)
      end, Map)) > 0.


% Prints the given Map
% Every cell is printed as tuple of own ships and own shots
% ( <ships> | <shots> )
% ( | ) : no ship       | not shot at cell on enemy side
% (O|-) : ship, not hit | water shot at cell on enemy side
% (X|+) : ship, hit     | hit ship on enemy side
% returns none
print_map(Map) ->
    print_map(0, 0, Map).
    
print_map(N, M, Map) ->
    if 
        % print column numbers
        N == 0 andalso M == 0 ->
            io:format("\ec"),
            io:format("~n    ", []),
            print_map(0, 1, Map);
        N == 0 andalso M > ?MAP_COLS  ->
            io:format("~n", []),
            print_map(1, 1, Map);
        N == 0 ->
            io:format("  ~w   ", [M]),
            print_map(N, M+1, Map);

        % print map
        N > ?MAP_ROWS -> 
            io:format("~n", []),
            ok;
        
        % next line
        N > 0 andalso M > ?MAP_COLS ->
            io:format("~n", []),
            print_map(N+1, 1, Map);
        
        % print map lines   
        N > 0 ->
            % print row numbers in left of first map column
            if 
                M == 1 -> io:format(" ~2w ", [N]);
                true   -> ok
            end,
            % print cells
            S = maps:get({N,M}, Map),
            case {lists:member(ship, S),lists:member(ship_shot, S)} of
                {true,false}  -> io:format("(O|", []);
                {_,true}      -> io:format("(X|", []);
                {false,false} -> io:format("( |", [])
            end,
            case {lists:member(shot_hit, S),lists:member(shot_water, S)} of
                {true,_}      -> io:format("+) ", []);
                {_,true}      -> io:format("-) ", []);
                {false,false} -> io:format(" ) ", [])
            end,
            print_map(N, M+1, Map)
    end.


% Init game, build map, place ships and wait for enemy to get ready
% PID   : ProcessPID of the EnemyProcess
% returns generated Map with placed ships
init_game(PID) ->
     % build map
    Map = init_map(),
    print_map(Map),

    % place ships
    io:format("Place your ships!~n"),
    Map_with_ships = place_ship(?NUM_SHIPS, Map),

    io:format("~nWaiting for enemy to get ready...~n"),
    PID ! ready,
    receive
        ready -> ok
    end,
    io:format("~nGO!~n"),
    Map_with_ships.


% Server main routine
% Prepare the shell for a new game and waiting for a client to connect
% After successfull connection with client, init the game and start shooting
% returns none
init_server() ->
    io:format("\ec"),
    io:format("Battleships - Server started with PID ~w~n", [self()]),
    % wait for client connection
    receive
        {hello, Client_PID} ->
            io:format("Client connected with PID ~w~n", [Client_PID]),
            Client_PID ! {world, self()}
    end,
    % init game and ships
    Map = init_game(Client_PID),

    % start game loop, server shots first
    shoot(Map, Client_PID).


% Client main routine
% Prepare the shell for a new game, connect to the given server node.
% After successfull connection with server, init the game and await the
% first shot from the server 
% returns none
init_client(Server_Node) -> 
    io:format("\ec"),
    io:format("Battleships - client started with PID ~w~n", [self()]),
    {server, Server_Node} ! {hello, self()},
    receive
        {world, Server_PID} ->
            io:format("Connected to server with PID ~w~n", [Server_PID])
    end,
    
    % init game and ships
    Map = init_game(Server_PID),

    % start game, server shots first
    receive_shot(Map, Server_PID).


% Main entry routine for server, spawning and registering a new server process
server() ->
    register(server, spawn(battleships, init_server, [])),
    ok.


% Main entry routine for client, spawning new client process to connect to
% given Server_Node
client(Server_Node) ->
    spawn(battleships, init_client, [Server_Node]),
    ok.
